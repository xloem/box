# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

## [1.1.4] - 2020-07-11

### Fixed

  * fix build target to (re)make recipes
  * nodes +extroverted +introverted:
    fix handle service ssh (not bogus sshd)
  * node +gateway:
    fix enable dhcp service

### Added

  * support arm64 devices a64 a64-emmc

### Changed

  * nodes core*: add class hw.firmware.nic
    (and stop explicitly include package firmware-ath9k-htc);
    tighten dependency on boxer-data
  * nodes core*: include package fdisk explicitly
    (it is no longer a core package since Bullseye)
  * nodes core*: drop comments about package debian-security-support:
    included with Admin.apt.tools since boxer-data 10.8.15
  * support arch- and di-specific device lists;
    have di avoid a64-emmc;
    have buster avoid a64 a64-emmc

## [1.1.3] - 2020-04-06

### Fixed

  * node +solidbox: fix syntax of sagan tweak
  * fix access to root rescue shell,
    thanks to Smokeysea <smokeysea@protonmail.com> (see bug#802211);
    tighten core profiles to require Buster or newer
    (rescue shell fix requires systemd 240)
  * fix resolve when to use qemu
    (fakeroot mode still broken: now hangs...)
  * update workaround for ca-certificates broken with QEMU (bug#923479):
    apparently previous workaround fails with recent glibc or openssl

### Changed

  * nodes core*: include class Console.editor (not Admin.tools)
  * nodes core*: stop include package modemmanager
  * node +solidbox: include package alsa-utils
  * node +solidbox: include package esekeyd; add TODO comments
  * limit each rsync copy to same filesystem

## [1.1.2] - 2020-01-21

### Added

  * add node +domotics

### Changed

  * node +solidbox: include packages mpd mpd-sima prosody radicale

## [1.1.1] - 2019-12-09

### Fixed

  * system-setup: fix offer to reinstall
  * system-setup: fix setup keyboard before offer to reinstall
    (needed for providing passphrase for full disk encryption)
  * system-setup: fix ask initial debconf questions at default priority
    (i.e. suppress by default)
  * system-setup: fix handle unresolvable package status

### Added

  * add node +fixed

### Changed

  * docs: highlight fewer words,
    to avoid mistaking as subsection
  * docs: fix drop lsblk option --nodeps,
    to show mountpoints
  * docs: cover target hardware requirements in USE (not SETUP)
  * docs: rewrite USE intro
  * docs: rewrite USE section Stion image

## [1.1.0] - 2019-12-04

### Fixed

  * system-setup: Fix resize root partition in bullseye
    (use sfdisk not parted).
  * system-setup: Fix skip netwok actions as needed.

### Added

  * system-setup: Optionally reinstall to another storage device.

### Changed

  * Use apt-get (not apt) in non-interactive routines.
  * Improve documentation.

## [1.0] - 2019-11-05

## [1.0rc7] - 2019-09-28

## [1.0rc6] - 2019-09-04

## [1.0rc5] - 2019-06-27

## [1.0rc4] - 2019-06-24

## [1.0rc3] - 2019-06-17

## [1.0rc2] - 2019-06-04

## [1.0rc1] - 2019-05-05

## [1.0b22] - 2019-05-02

## [1.0b21] - 2019-04-25

## [1.0b20] - 2019-04-24

## [1.0b19] - 2019-04-23

## [1.0b18] - 2019-04-03

## [1.0b17] - 2018-12-28

## [1.0b16] - 2018-10-19

## [1.0b15] - 2018-10-14

## [1.0b14] - 2018-10-13

## [1.0b13] - 2018-09-13

## [1.0b12] - 2018-07-13

## [1.0b11] - 2018-04-17

## [1.0b10] - 2017-11-28

## [1.0b9] - 2017-11-11

## [1.0b8] - 2017-11-09

## [1.0b7] - 2017-02-23

## [1.0b4] - 2016-11-07

## [1.0b3] - 2016-11-06

## [1.0b2] - 2016-11-03

## [1.0b1] - 2016-11-03

## [0.1.0] - 2015-08-27

### Added

  * Initial experimental release.

[Unreleased]: https://salsa.debian.org/tinker-team/box/compare/v1.1.4...master
[1.1.4]: https://salsa.debian.org/tinker-team/box/compare/v1.1.3...v1.1.4
[1.1.3]: https://salsa.debian.org/tinker-team/box/compare/v1.1.2...v1.1.3
[1.1.2]: https://salsa.debian.org/tinker-team/box/compare/v1.1.1...v1.1.2
[1.1.1]: https://salsa.debian.org/tinker-team/box/compare/v1.1.0...v1.1.1
[1.1.0]: https://salsa.debian.org/tinker-team/box/compare/v1.0...v1.1.0
[1.0]: https://salsa.debian.org/tinker-team/box/compare/1.0rc7...v1.0
[1.0rc7]: https://salsa.debian.org/tinker-team/box/compare/1.0rc6...1.0rc7
[1.0rc6]: https://salsa.debian.org/tinker-team/box/compare/1.0rc5...1.0rc6
[1.0rc5]: https://salsa.debian.org/tinker-team/box/compare/1.0rc4...1.0rc5
[1.0rc4]: https://salsa.debian.org/tinker-team/box/compare/1.0rc3...1.0rc4
[1.0rc3]: https://salsa.debian.org/tinker-team/box/compare/1.0rc2...1.0rc3
[1.0rc2]: https://salsa.debian.org/tinker-team/box/compare/1.0rc1...1.0rc2
[1.0rc1]: https://salsa.debian.org/tinker-team/box/compare/1.0b22...1.0rc1
[1.0b22]: https://salsa.debian.org/tinker-team/box/compare/1.0b21...1.0b22
[1.0b21]: https://salsa.debian.org/tinker-team/box/compare/1.0b20...1.0b21
[1.0b20]: https://salsa.debian.org/tinker-team/box/compare/1.0b19...1.0b20
[1.0b19]: https://salsa.debian.org/tinker-team/box/compare/1.0b18...1.0b19
[1.0b18]: https://salsa.debian.org/tinker-team/box/compare/1.0b17...1.0b18
[1.0b17]: https://salsa.debian.org/tinker-team/box/compare/1.0b16...1.0b17
[1.0b16]: https://salsa.debian.org/tinker-team/box/compare/1.0b15...1.0b16
[1.0b15]: https://salsa.debian.org/tinker-team/box/compare/1.0b14...1.0b15
[1.0b14]: https://salsa.debian.org/tinker-team/box/compare/1.0b13...1.0b14
[1.0b13]: https://salsa.debian.org/tinker-team/box/compare/1.0b12...1.0b13
[1.0b12]: https://salsa.debian.org/tinker-team/box/compare/1.0b11...1.0b12
[1.0b11]: https://salsa.debian.org/tinker-team/box/compare/1.0b10...1.0b11
[1.0b10]: https://salsa.debian.org/tinker-team/box/compare/1.0b9...1.0b10
[1.0b9]: https://salsa.debian.org/tinker-team/box/compare/1.0b8...1.0b9
[1.0b8]: https://salsa.debian.org/tinker-team/box/compare/1.0b7...1.0b8
[1.0b7]: https://salsa.debian.org/tinker-team/box/compare/1.0b6...1.0b7
[1.0b4]: https://salsa.debian.org/tinker-team/box/compare/1.0b3...1.0b4
[1.0b3]: https://salsa.debian.org/tinker-team/box/compare/1.0b2...1.0b3
[1.0b2]: https://salsa.debian.org/tinker-team/box/compare/1.0b1...1.0b2
[1.0b1]: https://salsa.debian.org/tinker-team/box/compare/8a0c889...1.0b1
[0.1.0]: https://salsa.debian.org/tinker-team/box/commits/8a0c889
