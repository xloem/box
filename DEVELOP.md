# Notes on developing box install routines


## Serial access

Serial access for OLinuXino boards needs UART-to-serial cable.
Either
https://www.olimex.com/Products/Components/Cables/USB-Serial-Cable/USB-Serial-Cable-F/
or an old Nokia DKU-5 or CA-42 data cable cannibalized like
http://buffalo.nas-central.org/index.php/Use_a_Nokia_Serial_Cable_on_an_ARM9_Linkstation

Connect to serial console - e.g. one of these:

    screen /dev/ttyUSB0 115200

    picocom -b 115200 /dev/ttyUSB0


## APT proxy

When testing, same packages are downloaded again and again.

An APT proxy like approx may then be beneficial.

To hardcode proxy info into installer image,
either export http_proxy=http://192.168.222.250:9999/
or create the file local.mk and put same info there
(replace with actual IP of proxy).


## Multiarch

If build fails to execute hook "completion_50_configscript.sh",
then try install dpkg-dev in the build environment
to resolve architecture-related variables most reliably.
