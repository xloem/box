# Optional file to set variables also possible to pass in environment:
# * http_proxy - proxy URL (for APT), e.g. http://example.net:9999/
# * apt_cacher_host - apt-cacher host (for all downloads), e.g. example.net
# * approx_host - approx host (for all downloads), e.g. example.net
# * noscreen - set to "true" to use default (serial, not HDMI) with d-i
# * noquiet - set to "true" to not quiet down default boot messages
# * nogfx - set to "true" to minimize video buffers
# * bootargs - set custom bootargs, e.g. BOOT_DEBUG=5 log_host=example.org preseed/interactive=true
# * fakeroot - command used for faked root (set empty to not fake root)
# * sudo - command used for real root (leave empty to not switch to root)
# * eatmydata - avoid fsync (set empty to not use)
# * linux - custom kernel: colon-separated suite and version
# * imgsuffix - suffix to append to basename of final image filename
# * page_KiB - MicroSD card page size in power-of-two kilobytes
# * multi-plane_KiB - MicroSD card multi-plane access size in power-of-two kilobyte
# * erase-block_MiB - MicroSD card erase block size in power-of-two megabyte
# * erase-block-di_MiB - d-i image erase block size in power-of-two megabyte
# * bootfs_MiB - boot partition size in power-of-two megabytes
-include local.mk

DATADIR ?= content

doc_DATADIR = $(DATADIR)
img_DATADIR = $(DATADIR)/images
recipe_DATADIR = $(DATADIR)/recipes

suites = buster bullseye sid
archs = armel armhf arm64
archs-di = armhf arm64
devices-armel = sheevaplug
devices-armhf = lime lime2 micro
devices-arm64 = a64 a64-emmc teres1
devices_di-arm64 = a64 teres1
devices_buster-arm64 = teres1
devices-nonfree = teres1
allnodefiles := $(wildcard nodes/*.yml)
allnodes := $(patsubst nodes/%.yml,%,$(allnodefiles))
addons := $(patsubst +%,%,$(filter +%,$(allnodes)))
nodes := $(filter-out $(foreach addon,$(addons),%$(addon)),$(allnodes))
recipes = $(nodes) $(patsubst %,nonfree_%,$(devices-nonfree))
recipedirs = $(foreach suite,$(suites),\
 $(patsubst %,$(recipe_DATADIR)/%-$(suite),$(recipes)))
addondirs = $(foreach suite,$(suites),\
 $(patsubst %,$(recipe_DATADIR)/+%-$(suite),$(addons)))
info = $(doc_DATADIR)/changes.html $(doc_DATADIR)/TODO
info += $(doc_DATADIR)/index.html $(doc_DATADIR)/style.css

this-recipedir = $(recipe_DATADIR)/$(1)-$(2)

this-recipedir-variant = $(firstword $(subst -, ,$(patsubst $(recipe_DATADIR)/%,%,$(1))))
this-recipedir-node = $(filter-out plain,\
 $(patsubst nonfree%,core%,\
  $(call this-recipedir-variant,$(1))))
this-recipedir-suite = $(patsubst %/,%,$(lastword $(subst -, ,$(1))))

this-title = $(shell grep -Pom1 '^\# \K.*[^\# ]+' $(1))

LINUX_SUITE = $(word 1,$(subst :, ,$(linux)))
LINUX_VERSION = $(word 2,$(subst :, ,$(linux)))
LINUX_SUFFIX = $(word 3,$(subst :, ,$(linux)))

#installsuite-buster = $(if $(foobar),buster))
#installsuite-buster-reason = $(if $(foobar),foobar broken with buster))

# * d-i releases require an archive with same-ABI kernel.
# * d-i rejects non-stable snapshot older than 1 month: bug#771699
#time-buster-reason = bug\#771699

#snapshot-buster-di = 20170127
#snapshot-buster-kernel = 4.9.0-1

# if no newer snapshot available for sid, reuse bullseye hints
#time-sid-reason = $(time-bullseye-reason)
snapshot-sid-di = $(snapshot-bullseye-di)
snapshot-sid-kernel = $(snapshot-bullseye-kernel)
#snapshot-sid-archive = $(snapshot-bullseye-archive)
#snapshot-sid-reason = $(snapshot-bullseye-reason)
#time-sid = $(time-bullseye)

this-partdir = imageparts/$(or $(installsuite-$(1):%=$(1)-%),$(1))
this-vendordir = $(foreach suite-di,$(or $(installsuite-$(1)),$(1)),$(strip \
 vendor/$(or $(snapshot-$(suite-di)-di:%=$(suite-di)-%),$(suite-di))))
this-snapshot = $(snapshot-$(or $(installsuite-$(1)),$(1))-archive)
this-snapshot-kernel = $(snapshot-$(or $(installsuite-$(1)),$(1))-kernel)
this-snapshot-reason = $(snapshot-$(or $(installsuite-$(1)),$(1))-reason)

proxy = $(or $(http_proxy),$(apt_cacher_host:%=http://%:3142/),$(approx_host:%=http://%:9999/))
this-repo = $(or $(http_proxy:%/=%),$(apt_cacher_host:%=http://%:3142/$(1)),$(approx_host:%=http://%:9999),http://$(1))$(2)

this-installsuite-warn = \
 WARNING:Installer $(installsuite-$(1)) used for target $(1)$(if $(installsuite-$(1)-reason),\
 ($(installsuite-$(1)-reason))).\n

this-snapshot-warn = \
 WARNING: Install uses snapshot archive$(if $(call this-snapshot-reason,$(1)),\
 ($(call this-snapshot-reason,$(1)))).\n$(if $(time-$(1)),\
 !!! Time will be set back during install$(if $(time-$1)-reason,\
 ($(time-$(1)-reason))).\n)\
 !!! At first boot of new system, edit /etc/apt/sources.list e.g. to this:\n\
 $()  deb http://deb.debian.org/debian/ $(1) main\n\
 $()  deb http://deb.debian.org/debian/ $(1)-updates main\n\
 $()  deb http://security.debian.org/ $(1) main\n

this-warnings = \
 $(if $(installsuite-$(1)),\
 printf "\n$(call this-installsuite-warn,$(1))";)\
 $(if $(call this-snapshot,$(1)),\
 printf "\n$(call this-snapshot-warn,$(1))")

di-scr-armhf = yes
di-extlinux-arm64 = yes
di-firmware-armhf-lime = A20-OLinuXino-Lime
di-firmware-armhf-lime2 = A20-OLinuXino-Lime2
di-firmware-armhf-micro = A20-OLinuXino-MICRO
di-firmware-arm64-a64 = a64-olinuxino
di-firmware-arm64-a64-emmc = a64-olinuxino-emmc
di-firmware-arm64-teres1 = teres_i
spl-armhf-lime = A20-OLinuXino-Lime/u-boot-sunxi-with-spl.bin
spl-armhf-lime2 = A20-OLinuXino-Lime2/u-boot-sunxi-with-spl.bin
spl-armhf-micro = A20-OLinuXino_MICRO/u-boot-sunxi-with-spl.bin
atf-arm64-a64 = a64-olinuxino
atf-arm64-a64-emmc = a64-olinuxino-emmc
atf-arm64-teres1 = teres_i
dtb-armel-sheevaplug = kirkwood-sheevaplug.dtb
dtb-armhf-lime = sun7i-a20-olinuxino-lime.dtb
dtb-armhf-lime2 = sun7i-a20-olinuxino-lime2.dtb
dtb-armhf-micro = sun7i-a20-olinuxino-micro.dtb
dtb-arm64-a64 = allwinner/sun50i-a64-olinuxino.dtb
dtb-arm64-a64-emmc = allwinner/sun50i-a64-olinuxino-emmc.dtb
dtb-arm64-teres1 = allwinner/sun50i-a64-teres-i.dtb
scr-armel = bootscr.uboot-generic
scr-armhf = bootscr.sunxi
scr-arm64 = bootscr.uboot-generic

this-atf = TARGET="$(CURDIR)/$(1)/usr/lib/u-boot/$(3)" \
 BL31="$(CURDIR)/$(1)/usr/lib/arm-trusted-firmware/sun50i_a64/bl31.bin" \
 FIT_GENERATOR="$(CURDIR)/$(1)/usr/bin/mksunxi_fit_atf" \
 $(1)/usr/bin/u-boot-install-sunxi64 "$(CURDIR)/$(2)~"

host-arch := $(shell dpkg --print-architecture)

vendor-files = $(foreach suite,$(suites),\
 $(foreach arch,$(archs),\
 $(foreach device,$(or $(devices_di_$(suite)-$(arch)),$(devices_$(suite)-$(arch)),$(devices_di-$(arch)),$(devices-$(arch))),\
 vendor/$(or $(snapshot-$(suite)-di:%=$(suite)-%),$(suite))/$(arch)/$(device))))
images-di = $(foreach suite,$(suites),\
 $(foreach arch,$(archs-di),\
 $(foreach device,$(or $(devices_di_$(suite)-$(arch)),$(devices_$(suite)-$(arch)),$(devices_di-$(arch)),$(devices-$(arch))),\
  $(img_DATADIR)/plain/plain-$(device)-$(suite)$(imgsuffix).img.gz)))
images-local = $(foreach suite,$(filter-out sid,$(suites)),\
 $(foreach arch,$(if $(fakeroot),$(host-arch),$(archs)),\
 $(foreach device,$(or $(devices_$(suite)-$(arch)),$(devices-$(arch))),\
 $(if $(filter $(device),$(devices-nonfree)),\
  $(img_DATADIR)/nonfree/nonfree-$(device)-$(suite)$(imgsuffix).img.gz) \
  $(img_DATADIR)/core/core-$(device)-$(suite)$(imgsuffix).img.gz)))

this-kernelbuild = $(shell readlink $(1) | grep -Po 'vmlinuz-\K.*')

page_KiB ?= 4
erase-block_MiB ?= 4
erase-block-di_MiB ?= 1

# tune ext4 formatting when multi-plane access size is set (maybe equal to page)
# https://wiki.gentoo.org/wiki/SDCard#Solution_2:_Tuned_ext4
# https://thelastmaimou.wordpress.com/2013/05/04/magic-soup-ext4-with-ssd-stripes-and-strides/
# https://lists.linaro.org/pipermail/flashbench-results/2014-July/000479.html
ifneq (,$(multi-plane_KiB))
_block := $(shell echo $$(( $(page_KiB) * 1024 )) )
_stride := $(shell echo $$(( $(multi-plane_KiB) / $(page_KiB) )) )
_stripe := $(shell echo $$(( $(erase-block_MiB) * 1024 / $(page_KiB) )) )
_tune-ext4 = -b $(_block)$(_stride:%= -E stride=%$(_stripe:%=,stripe-width=%))
endif

# boot partition size, in power-of-two megabytes
bootfs_MiB ?= 200

# shell calculation snippet to expand to nearest erase block
this-fit-erase-block = ( $(1) / $(erase-block_MiB) + 1 ) * $(erase-block_MiB)

bootfs_MiB := $(shell echo $$(( $(call this-fit-erase-block,$(bootfs_MiB)) )) )

# use video console (HDMI/VGA) by default (U-boot default is serial)
console = $(strip $(if $(noscreen),\
 ,\
 tty0))

# ensure custom bootargs are applied last
bootargs := $(strip \
 $(if $(nogfx),\
  sunxi_ve_mem_reserve=0 \
  sunxi_g2d_mem_reserve=0 \
  sunxi_fb_mem_reserve=16 \
 ) \
 $(bootargs))

SUPATH = PATH=/usr/sbin:/sbin:$(PATH)

# tool is either custom (even empty), in PATH, fallback in PATH, or missing
this_tool = $(strip \
 $(if $(filter-out undefined,$(origin $(1))),\
  $($(1)),\
  $(or \
   $(if $(wildcard $(patsubst %,%/$(1),$(subst :, ,$(PATH)))),\
    $(1)),\
   $(if $(wildcard $(patsubst %,%/$(2),$(subst :, ,$(PATH)))),\
    $(2)$(eval tools_fallback += $(1))),\
   $(eval tools_missing += $(1)$(3:%=[%])))))

GZIP := $(call this_tool,pigz,gzip)
GUNZIP := $(call this_tool,unpigz,gunzip)

FAKEROOT := $(call this_tool,fakeroot)
SUDO := $(call this_tool,sudo)
this-fakeroot = $(if $(FAKEROOT),$(call this-fakeroot-$(FAKEROOT),$(1)),$(SUDO))
this-fakeroot-fakeroot = fakeroot -i'$(1)' -s'$(1)'
this-fakeroot-fakeroot-ng = fakeroot-ng -p'$(1)'
this-fakeroot-fakeroot-pseudo = fakeroot-pseudo -i'$(1)' -s'$(1)'

MOUNT_FAT := $(call this_tool,fusefat) -o rw+
UMOUNT_FAT = fusermount -u

# TODO: avoid sudo when fuse2fs proven reliable
MOUNT_EXT := $(if $(SUDO),$(SUDO) mount,$(SUPATH) $(call this_tool,fuse2fs))
UMOUNT_EXT = $(if $(SUDO),$(SUDO) umount,fusermount -u)

# options fetched from "bash -x flash-kernel" on Lime2 box.
MKIMAGE := $(call this_tool,mkimage,,sunxi-tools) -A arm -O linux -T script -C none -a '' -e '' -n 'boot script'

EATMYDATA := $(call this_tool,eatmydata)

$(if $(tools_fallback),\
 $(warning NOTICE: Helper tools missing (replaced by alternative):\
 $(tools_fallback)))

$(if $(tools_missing),\
 $(warning WARNING: Helper tools missing:\
 $(tools_missing)))

all: $(images-di) $(images-local) $(info)

images-di: $(images-di)
images-local: $(images-local)

# TODO: drop sid → bullseye workaround when fixed in boxer
$(recipedirs) $(addondirs): \
 $(allnodefiles) skel/preseed.cfg.in skel/script.sh.in
	$(eval suite = $(call this-recipedir-suite,$@))
	$(eval node = $(call this-recipedir-node,$@))
	$(eval variant = $(call this-recipedir-variant,$@))
	$(if $(wildcard $@),find $@ \( -name preseed.cfg -or -name script.sh -or -name '*.sig' \) -delete)
	$(if $(wildcard $@),rmdir $@)
	mkdir -p $@~
	cd $@~ \
		&& boxer compose \
			--nodedir $(CURDIR)/nodes \
			--skeldir $(CURDIR)/skel \
			--suite $(patsubst sid,bullseye,$(suite)) \
			$(if $(filter nonfree%,$(variant)),--nonfree) \
			$(node)
	$(if $(filter nonfree%,$(variant)),\
		mv $@~/preseed.cfg $@~/preseed-nonfree.cfg)
	$(if $(filter nonfree%,$(variant)),\
		mv $@~/script.sh $@~/script-nonfree.sh)
	@mv $@~ $@

$(info): $(doc_DATADIR)/% : %
	mkdir -p $(doc_DATADIR)
	cp -f $< $@

index.html: USE.md style.css
	( printf '%% $(call this-title,$<)\n\n'; tail -n +2 $< ) | \
		sed -e 's/buster\.img/buster$(imgsuffix).img/;' | \
		pandoc -s \
			-c style.css \
			-f markdown -t html \
			-o $@

changes.html: CHANGELOG.md style.css
	( printf '%% $(call this-title,$<)\n\n'; tail -n +2 $< ) | \
		pandoc -s \
			-c style.css \
			-f markdown -t html \
			-o $@

style.css: style.scss
	sassc \
		--load-path /usr/share/sass/stylesheets \
		--load-path /usr/share/compass/frameworks/h5bp/stylesheets \
		--style compressed \
		$< $@

clean::
	rm -rf $(DATADIR)

define SUITE_ARCH_DEVICE_template =
# Prepend firmware to (plain/preseed installer/bootstrapped) partition
$(img_DATADIR)/plain/plain-$(3)-$(1)$(imgsuffix).img.gz: \
 $(call this-vendordir,$(1))/$(2)/firmware.$(di-firmware-$(2)-$(3)).img.gz \
 $(call this-partdir,$(1))/$(2)/plain/partition.img.gz
	mkdir -p $$(dir $$@)
	zcat $$^ | $(GZIP) --rsyncable > $$@~
	mv -f $$@~ $$@
	@$(call this-warnings,$(1))
endef
$(foreach suite,$(suites),\
 $(foreach arch,$(archs),\
 $(foreach device,$(or $(devices_$(suite)-$(arch)),$(devices-$(arch))),\
 $(eval $(call SUITE_ARCH_DEVICE_template,$(suite),$(arch),$(device))))))

define SUITE_ARCH_DEVICE_VARIANT_template =
$(img_DATADIR)/$(firstword $(subst _,$() ,$(4)))/$(firstword $(subst _,$() ,$(4)))-$(3)-$(1)$(imgsuffix).img.gz: \
 $(call this-partdir,$(1))/$(2)/$(4)/local/firmware.$(3).img \
 partition/$(1)/$(2)/$(4)/bootfs.img \
 partition/$(1)/$(2)/$(4)/rootfs.img
	mkdir -p $$(dir $$@)
	cat $$^ | $(GZIP) --rsyncable > $$@~
	mv -f $$@~ $$@
	@$(call this-warnings,$(1))

# Compose firmware from bootstrapped system
$(call this-partdir,$(1))/$(2)/$(4)/d-i/firmware.$(3).img: \
 partition/$(1)/$(2)/$(4)/rootfs
	mkdir -p $$(dir $$@)
	dd if=/dev/zero of=$$@~ bs=1M count=0 seek=39
	$(SUPATH) parted -s $$@~ -- \
		mklabel msdos \
		mkpart primary fat32 1MiB 100% \
		toggle 1 boot
	truncate -s $(erase-block-di_MiB)M $$@~
	$(strip $(if $(atf-$(2)-$(3)),\
		$$(if $$(wildcard $$</usr/lib/u-boot/$(atf-$(2)-$(3))),\
			$$(call this-atf,$$<,$$@,$(atf-$(2)-$(3))))))
	$(strip $(if $(spl-$(2)-$(3)),\
		dd conv=notrunc bs=8k seek=1 if=$$</usr/lib/u-boot/$(spl-$(2)-$(3)) of=$$@~))
	mv -f $$@~ $$@

# Boot partition
# TODO: extract boot.cmd variables from rootfs
partition/$(1)/$(2)/$(4)/bootfs.img: \
 partition/$(1)/$(2)/$(4)/rootfs
	$$(eval kernelbuild = $$(call this-kernelbuild,$$</vmlinuz))
	mkdir -p $$(dir $$@)
	$(SUPATH) mkfs.ext4 $(_tune-ext4) $$@~ $(bootfs_MiB)M
	mkdir -p $$@.mnt
	$(MOUNT_EXT) $$@~ $$@.mnt
	$(or $(SUDO),$(call this-fakeroot,partition/$(1)/$(2)/$(4)/rootfs.state)) \
		chown root: $$@.mnt
	$(or $(SUDO),$(call this-fakeroot,partition/$(1)/$(2)/$(4)/rootfs.state)) \
		cp --archive \
			--target-directory $$@.mnt \
			-- $$</boot/*
	$(UMOUNT_EXT) "$$@.mnt"
	mv -f $$@~ $$@

$(call this-partdir,$(1))/$(2)/$(4)/local/firmware.$(3).img: \
 partition/$(1)/$(2)/$(4)/rootfs \
 partition/$(1)/$(2)/$(4)/rootfs.size
	mkdir -p $$(dir $$@)
	dd if=/dev/zero of=$$@~ \
		bs=1M \
		count=0 \
		seek=$$$$(( \
			$(erase-block_MiB) \
			+ $(bootfs_MiB) \
			+ $$$$(cat partition/$(1)/$(2)/$(4)/rootfs.size) \
				/ 1024 / 1024 ))
	$(SUPATH) parted -s $$@~ -- \
		mklabel msdos \
		mkpart primary ext2 \
			$(erase-block_MiB)MiB \
			$$$$(( ( \
				$(erase-block_MiB) + $(bootfs_MiB) ) * 1024 * 1024 \
				- 1 ))B \
		toggle 1 boot \
		mkpart primary ext2 \
			$$$$(( ( $(erase-block_MiB) + $(bootfs_MiB) ) * 1024 * 1024 \
				))B \
			100%
	truncate -s $(erase-block_MiB)M $$@~
	$(strip $(if $(atf-$(2)-$(3)),\
		$$(if $$(wildcard $$</usr/lib/u-boot/$(atf-$(2)-$(3))),\
			$$(call this-atf,$$<,$$@,$(atf-$(2)-$(3))))))
	$(strip $(if $(spl-$(2)-$(3)),\
		dd conv=notrunc bs=8k seek=1 if=$$</usr/lib/u-boot/$(spl-$(2)-$(3)) of=$$@~))
	mv -f $$@~ $$@
	touch $$@

# Add preseed file to ramdisk in installer partition
$(call this-partdir,$(1))/$(2)/$(4)/partition.img.gz: \
 $(call this-partdir,$(1))/$(2)/plain/partition.img.gz \
 $(call this-recipedir,$(4),$(1))
	mkdir -p $$(dir $$@)
	cp -f $$< $$@
	$(GUNZIP) -f $$@
	mkdir -p $$(dir $$@)ramdisk
	$(MOUNT_FAT) $$(patsubst %.gz,%,$$@) $$(dir $$@)ramdisk
	cp -f $$(dir $$@)ramdisk/initrd.gz $$(dir $$@)
	$(GUNZIP) -f $$(dir $$@)initrd.gz
	cp -f $(call this-recipedir,$(4),$(1))/preseed$(if $(filter nonfree%,$(4)),-nonfree).cfg $$(dir $$@)preseed.cfg
	cd $$(dir $$@) && echo preseed.cfg | cpio --format=newc -oAO initrd
	$(GZIP) -f $$(dir $$@)initrd
	rm -f $$(dir $$@)ramdisk/INITRD.GZ
	cp $$(dir $$@)initrd.gz $$(dir $$@)ramdisk/INITRD.GZ
	$(UMOUNT_FAT) $$(dir $$@)ramdisk
	$(GZIP) -f $$(patsubst %.gz,%,$$@)
endef
$(foreach suite,$(suites),\
 $(foreach arch,$(archs),\
 $(foreach device,$(or $(devices_$(suite)-$(arch)),$(devices-$(arch))),\
 $(foreach variant,$(filter %_$(device),$(recipes)),\
 $(eval $(call SUITE_ARCH_DEVICE_VARIANT_template,$(suite),$(arch),$(device),$(variant)))))))

define SUITE_ARCH_VARIANT_template =
# Copy preseed file
partition/$(1)/$(2)/$(3)/rootfs.preseed.cfg: \
 $(call this-recipedir,$(3),$(1))
	mkdir -p "$$(dir $$@)"
	$(if $(filter nonfree%,$(3)),\
		cp -f $(call this-recipedir,$(3),$(1))/preseed-nonfree.cfg $$@,\
		cp -f $(call this-recipedir,$(3),$(1))/preseed.cfg $$@)
	$$(if $$(LINUX_VERSION),\
		perl -i -pe 's/linux-image\K(-\S+)/-$$(LINUX_VERSION)$$$$1$$(LINUX_SUFFIX)/' \
			$$@)

# Configure multistrap
# TODO: install libfakeroot only on alien arch
# TODO: Stop uncomment hooks when faketime supports chroot: bug#778462
partition/$(1)/$(2)/$(3)/rootfs.multistrap.conf: \
 partition/$(1)/$(2)/$(3)/rootfs.preseed.cfg \
 skel/multistrap.conf.in
	$$(eval packages = $$(strip \
		$(if $(FAKEROOT),libfakeroot) \
		$(if $(EATMYDATA),libeatmydata1) \
		$(if $(time-$(1)),libfaketime) \
		$$(shell perl -0777 -n \
			-e 's/ \\\n / /;' \
			-e 'print m,^d-i pkgsel/include string ([^\n]*),m;' \
			< $$<)))
	$$(eval kernelpackage = $$(strip \
		$$(shell grep -Po \
			'linux-image-$$(LINUX_VERSION)-\S+$$(LINUX_SUFFIX)' \
			$$<)))
	$(if $(call this-snapshot,$(1)),\
		$$(eval source = $(call this-repo,snapshot.debian.org,/archive/debian/$(call this-snapshot,$(1)))),
		$$(eval source = $(call this-repo,deb.debian.org,/debian)))
	perl -p \
		-e 's!\@suite\@!$(1)!;' \
		-e 's!\@kernelsuite\@!$(filter-out $(1),$(LINUX_SUITE))!g;' \
		-e 's!\@arch\@!$(2)!;' \
		-e 's!\@libdir\@!skel!;' \
		-e 's!\@preseed\@!$$<!;' \
		-e 's!\@packages\@!$$(packages)!;' \
		-e 's!\@kernelpackage\@!$$(kernelpackage)!g;' \
		-e 's!\@source\@!$$(source)!;' \
		$(if $(time-$(1)),\
			,\
			-e 's!^hookdir=!#hookdir=!g;') \
		$(if $(filter nonfree%,$(3)),\
			-e 's!^components=main\K! contrib non-free!;') \
		$$(if $$(and $(filter-out $(1),$(LINUX_SUITE)), $$(kernelpackage)),\
			-e 's!^bootstrap=\K!Kernel-$(LINUX_SUITE) !;') \
		< skel/multistrap.conf.in \
		> $$@

# Bootstrap system
# TODO: Stop manually run hooks when faketime supports chroot: bug#778462
partition/$(1)/$(2)/$(3)/rootfs.img.uuid: \
 partition/$(1)/$(2)/$(3)/rootfs.img
	$(SUPATH) blkid $$< | grep -Po 'UUID="\K[^"]+' > $$@~
	mv -f $$@~ $$@
partition/$(1)/$(2)/$(3)/rootfs.img: \
 partition/$(1)/$(2)/$(3)/rootfs \
 partition/$(1)/$(2)/$(3)/rootfs.size \
 bin/system-setup \
 $(filter %-$(1),$(addondirs))
	$(SUPATH) mkfs.ext4 $(_tune-ext4) -T default $$@~ \
		$$$$(( $$$$(cat "$$<.size") / 1024 ))k
	mkdir -p $$@.mnt
	$(MOUNT_EXT) $$@~ $$@.mnt
	$(or $(SUDO),$(call this-fakeroot,$$<.state)) \
		rsync --archive --acls --xattrs --sparse --numeric-ids --one-file-system --info=progress2 \
			$(and $(SUDO),$(FAKEROOT),\
				-e "$(call this-fakeroot,$$<.state)") \
			--exclude '/boot' \
			--exclude '/var/cache/apt/archives/*.deb' \
			--exclude multistrap-configure.sh \
			--exclude tmp/preseeds \
			$$</ $$@.mnt/
	$(or $(SUDO),$(call this-fakeroot,$$<.state)) \
		install -D --target-directory=$$@.mnt/usr/local/sbin \
			bin/system-setup
	$(foreach addondir,$(filter %-$(1),$(addondirs)),\
		$(or $(SUDO),$(call this-fakeroot,$$<.state)) \
			install --no-target-directory \
				$(addondir)/script.sh \
				$$@.mnt/usr/local/sbin/box-add-$(patsubst +%-$(1),%,$(notdir $(addondir)));)
	$(UMOUNT_EXT) "$$@.mnt"
	mv -f $$@~ $$@
	touch $$@

# Guestimate image size from size of contents
# * added 50% - 5 as reserved by ext4, and 45 for inaccuracy
partition/$(1)/$(2)/$(3)/rootfs.size: \
 partition/$(1)/$(2)/$(3)/rootfs
	echo \
		$$$$(( \
			$(call this-fit-erase-block,\
				$$$$( \
					$(call this-fakeroot,$$<.state) \
						du --bytes --summarize "$$<" \
							| cut -f 1 ) \
				/ 1024 / 1024 \
				/ 100 * 150 ) \
				* 1024 * 1024 )) \
		> $$@~
	mv -f $$@~ $$@

partition/$(1)/$(2)/$(3)/rootfs: \
 partition/$(1)/$(2)/$(3)/rootfs.multistrap.conf
	$(SUPATH) $(call this-fakeroot,$$@.state) \
		$(EATMYDATA) \
		$(if $(time-$(1)),\
			faketime $(time-$(1))) \
		multistrap \
			-f $$< \
			-d "$$@"
	$(if $(time-$(1)),\
		,\
		$(call this-fakeroot,$$@.state) \
			$(EATMYDATA) \
			run-parts \
				--regex '^completion' \
				--arg "$$@" \
				skel/multistrap-hooks)
# TODO: why doesn't this work?!?
#partition/$(1)/$(2)/$(3)/rootfs/%: \
# partition/$(1)/$(2)/$(3)/rootfs
endef
$(foreach suite,$(suites),\
 $(foreach arch,$(archs),\
 $(foreach variant,\
  $(filter $(foreach device,$(or $(devices_$(suite)-$(arch)),$(devices-$(arch))),%_$(device)),$(recipes)),\
 $(eval $(call SUITE_ARCH_VARIANT_template,$(suite),$(arch),$(variant))))))

define SUITE_ARCH_template =
# Adjust options in binary U-boot file boot.scr in ramdisk in partition:
# * Overwrite whole disk by default
# * if needed by installer, then
#   * hardcode use of snapshot archive
#   * avoid updates or security archives (unsupported with snapshot)
#   * if needed, set back time and suppress use of network time
#   else use deb.d.o by default
# * if variable "http_proxy", "apt_cacher_host" or "approx_host" is set,
#   hardcode http proxy
# * if non-default d-i suite in use, hardcode target suite
# * If U-boot uses HDMI+USB, have Linux not replace framebuffer driver
# * if variable "botargs" is set, append its content to bootargs
$(call this-partdir,$(1))/$(2)/plain/partition.img.gz: \
 $(call this-vendordir,$(1))/$(2)/partition.img.gz
	$$(eval bootargs := $$(strip \
		$(if $(console),\
			fb=false) \
		partman-auto/method?=regular \
		$$(if $$(call this-snapshot,$(1)),\
			mirror/country=manual \
			mirror/http/hostname=$(or $(apt_cacher_host:%=%:3142),$(approx_host:%=%:9999),snapshot.debian.org) \
			mirror/http/directory=$(apt_cacher_host:%=/snapshot.debian.org)/archive/debian/$$(call this-snapshot,$(1)) \
			apt-setup/services-select= \
			$$(if $$(time-$(1)),\
				preseed/early_command=\\"date -s $$(time-$(1))\\" \
				clock-setup/ntp=false) \
		,\
			mirror/country?=manual \
			mirror/http/hostname?=deb.debian.org \
			mirror/http/directory?=/debian \
		) \
		$(if $(proxy),\
			mirror/http/proxy=$(proxy)) \
		$$(if $$(installsuite-$(1)),\
			mirror/suite=$(1)) \
		$(bootargs)))
	mkdir -p $$(dir $$@)
	cp -f $$< $$@
	$(GUNZIP) -f $$@
	mkdir -p $$(dir $$@)ramdisk
	$(MOUNT_FAT) $$(patsubst %.gz,%,$$@) $$(dir $$@)ramdisk
	$(strip $(if $(di-scr-$(2)),\
		perl -0777 -p \
			-e 's/^.*(?=#\s*Bootscript)//s;' \
			$(if $(console),\
				-e 's/^(?=[^#])/setenv console $(console)/m;') \
			-e 's'\''^setenv bootargs \K'\''$$(bootargs)'\'';' \
			< $$(dir $$@)ramdisk/boot.scr \
			> $$(dir $$@)boot.cmd))
	$(strip $(if $(di-scr-$(2)),\
		$(MKIMAGE) -d $$(dir $$@)boot.cmd $$(dir $$@)boot.scr))
	$(strip $(if $(di-scr-$(2)),\
		rm -f $$(dir $$@)ramdisk/BOOT.SCR))
	$(strip $(if $(di-scr-$(2)),\
		cp $$(dir $$@)boot.scr $$(dir $$@)ramdisk/BOOT.SCR))
	$(strip $(if $(di-extlinux-$(2)),\
		sed \
			-e '/^initrd / aappend $(if $(console),console $(console) )$$(bootargs);' \
			< $$(dir $$@)ramdisk/EXTLINUX/extlinux.conf \
			> $$(dir $$@)extlinux.conf))
	$(strip $(if $(di-extlinux-$(2)),\
		rm -f $$(dir $$@)ramdisk/EXTLINUX/extlinux.conf))
	$(strip $(if $(di-extlinux-$(2)),\
		cp $$(dir $$@)extlinux.conf $$(dir $$@)ramdisk/EXTLINUX/extlinux.conf))
	$(UMOUNT_FAT) $$(dir $$@)ramdisk
	$(GZIP) -f $$(patsubst %.gz,%,$$@)

# Fetch pristine parts
vendor/$(or $(snapshot-$(1)-di:%=$(1)-%),$(1))/$(2)/%:
	mkdir -p $$(dir $$@)
	curl $(proxy:%=-x %) -fsSRo '$$@' \
		'http://ftp.de.debian.org/debian/dists/$(1)/main/installer-$(2)/$(or $(snapshot-$(1)-di),current)/images/netboot/SD-card-images/$$(notdir $$@)'
endef
$(foreach suite,$(suites),\
 $(foreach arch,$(archs),\
 $(eval $(call SUITE_ARCH_template,$(suite),$(arch)))))

#%.img.gz: %.img
#	$(GZIP) -c --rsyncable $< > $@~
#	mv -f $@~ $@

clean::
	$(SUDO) rm -rf --one-file-system images imageparts partition
	rm -f *.html *.css

distclean: clean
	rm -rf vendor

.PHONY: all clean distclean images-di images-local
.PRECIOUS: $(vendor-files)
