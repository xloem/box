# Plan for pre-boxed Debian systems _(box)_

Immediate focus is to support core bootstrapping of relevant hardware,
and provide addon scripts covering bootstrapping for relevant tasks.

Further down the road,
improve bootstrapping and cover also maintenance of relevant tasks.

Goal is to adopt all functionality into the _Boxer_ project,
and then terminate this project.


## Secured workstation

Reasonably secure system
suitable for handling sensitive data
and discretely handling cryptographic private keys,
e.g. personal workstation or administrator of other systems.


### Bootstrap workstation

  * Install core image
  * Enable full disk encryption
  * Create PGP key
  * Create Monkeysphere admin key
  * Create clevis admin key
  * Publish PGP key
  * Publish Monkeysphere admin key
  * Publish clevis admin key


### Backup workstation

  * Distribute sharded PGP private key to trustees
  * Distribute PGP revocation certificate to trustees
  * Make PGP-encrypted content backup


### Restore workstation

  * Install core image
  * Enable full disk encryption
  * Restore PGP key from trustees
  * Restore content
  * Revoke old Monkeysphere admin key
  * Create new Monkeysphere admin key
  * Publish Monkeysphere admin key


## Administrated server

Reasonably secure system
suitable for handling sensitive data
protected by remotely unlocked cryptographic keys,
e.g. servers managed by dedicated administrators.


### Bootstrap server

  * Install core image
  * Enable full disk encryption (with clevis?)
  * Add identity service
  * Enable Monkeysphere access, and disable password-based access
  * Create Monkeysphere host key
  * Publish Monkeysphere host key
  * Activate Monkeysphere-secured login


## Autonomous server

Reasonably secure system
suitable for autonomously handling sensitive data
protected by remotely unlocked cryptographic keys,
e.g. servers running without any dedicated administration.


### Bootstrap server

  * Install core image
  * Enable full disk encryption (triggered by type of initial login?)
  * Auto-add identity service
  * Auto-enable WebID access, and auto-disable password-based access
  * Auto-add Solid services
