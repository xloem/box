# Box install routines

This document covers routines to setup boxes using [Boxer][].

The install routines themselves are avaiable in a [git repository][].

Example commands in this text creates a Debian buster system on a lime2 box -
adjust suite and device as needed.

Beware that some newer devices or older systems require workarounds,
e.g. to avoid need for serial access during install.
Make sure to read eventual warnings spewed during image build.

[Boxer]: <https://wiki.debian.org/Boxer>
  "Boxer - tool for system deployment and for composing blends"

[git repository]: <https://salsa.debian.org/tinker-team/box>
  "Box project git repository"


## Preparations

### Hardware

Building images is mostly tested on amd64 hardware
using Qemu for cross-building,
but should work on any architecture
(at least when targeting same architecture).

For supported target hardware,
see `USE.md`.


### Environment

Build environment should be Debian Sid or Bullseye,
with these packages installed:

    apt install make curl boxer cpio u-boot-tools fusefat pigz eatmydata fakeroot multistrap parted udev proot qemu-user-static fuse2fs rsync sudo arch-test sassc sass-stylesheets-compass compass-normalize-plugin

Other environments may work too,
just please mention loudly any deviation when reporting bugs.

Root access is *not* required for all tasks,
only for above package installation,
for builds targeted alien architectures,
and for writing final image to microSD card.


## Install

Installation can be either of the following methods:

<!-- FIXME: not all working currently
  * manual - boot into debian-installer
  * automated - boot into debian-installer, with some answers pre-answered
  * non-interactive - boot into debian-installer, fully automated
  * preinstalled - boot into fully installed system
  * Mass-deployable - boot into system adapting itself on initial boot
-->

  * manual - boot into debian-installer
  * preinstalled - boot into fully installed system

Target system can be Debian Buster or Bullseye or Sid,
for either of the following uses:

  * netinst - as close to pristine debian-installer as possible
  * core - minimal base system

The core system ships with a selection of box-add-* scripts:

  * admin - administration tools
  * doc - documentation
  * serial - serial-only console (conflicts with tuimouse)
  * tui - text-based console
  * tuimouse - mouse support for text console (conflicts with serial)
  * gui - graphical console
  * gui_gnome - GNOME desktop environment
  * gui_gnustep - GNUStep desktop environment
  * gui_i3 - graphical i3 environment
  * gui_kde - KDE desktop environment
  * gui_lxde - LXDE dekstop environment
  * gui_lxqt - LXQt desktop environment
  * gui_mate - MATE desktop environment
  * gui_sugar - graphical Sugar learning environment
  * gui_ukui - UKUI desktop environment
  * guiterm - graphical terminal emulator
  * mobile - location-change and offline and downtime handling
  * extraverted - sociable networking (replaces/overrides introverted)
  * introverted - restrained networking (replaces/overrides extraverted)
  * gateway - masquerading gateway router
  * freedombox - server-oriented system setup as a [FreedomBox][]
  * solidbox - server-oriented system setup as a [SolidBox][] (early draft!)
  * lesser - development system for [Lesser][]

[FreedomBox]: <https://wiki.debian.org/FreedomBox/>
  "FreedomBox - self-hosting web server to deploy social applications on small machines"

[SolidBox]: <https://solidbox.org/>
  "SolidBox - self-hosting responsive server to deploy Solid and conventional services on small machines"

[Lesser]: <http://source.free-owl.de/map.git/tree/README.md>
  "Lesser - slippy maps visualizing societal activities (currently: traffic accidents in one german district)"


### Manual

Debian installation is by default interactive.

  * Fetch and merge installer image:

        make clean
        make content/images/plain/plain-lime2-buster.img.gz

  * Install (as root) installer onto microSD card
    (replace SDCARD with actual device):

        zcat content/images/plain/plain-lime2-buster.img.gz > /dev/SDCARD

  * Boot box with SDCARD plugged in,
    and follow instructions,
    especially this:

    * Pick one of the options to overwrite the whole disk
      (installer will be replaced by actual system on SDCARD)

  * Create install script:

        make clean
        make content/recipes/core-buster

  * Transfer content/core-buster/script.sh to box,
    and execute it as root.


<!-- FIXME: Not currently working!

### Automated

Official Debian,
pre-seeded with our package selection and configuration,
installed onto target box,
asking only most essential questions.

  * Fetch and merge installer parts
    with locally created preseeding file embedded:

        make clean
        bootargs=preseed/interactive=true make content/images/seeded/seeded-lime2-buster.img.gz

  * Install (as root) installer onto microSD card
    (replace SDCARD with actual device):

        zcat content/images/seeded/seeded-lime2-buster.img.gz > /dev/SDCARD

  * Boot box with SDCARD plugged in
    (installer will be replaced by actual system on SDCARD)


### Non-interactive

Official Debian,
pre-seeded with our package selection and configuration,
installed fully automated onto target box.

  * Fetch and merge installer parts
    with locally created preseeding file embedded:

        make clean
        make content/images/seeded/seeded-lime2-buster.img.gz

  * Install (as root) installer onto microSD card
    (replace SDCARD with actual device):

        zcat content/images/seeded/seeded-lime2-buster.img.gz > /dev/SDCARD

  * Boot box with SDCARD plugged in
    (installer will be replaced by actual system on SDCARD)

-->


### Preinstalled

FIXME: Requires root (i.e. add line "fakeroot=" to file local.mk)

Official Debian,
preinstalled as intermediary, locally deployable image.

  * Create image:

        make clean
        make content/images/core/core-lime2-buster.img.gz

  * Install (as root) installer onto microSD card
    (replace SDCARD with actual device):

        zcat content/images/core/core-lime2-buster.img.gz > /dev/SDCARD

  * Boot box with SDCARD plugged in


<!-- FIXME: Not yet implemented!

### Mass-deployable

Official Debian,
preinstalled as intermediary generic image.

  * Create image:

        make clean
        make content/images/transit/transit-lime2-buster.img.gz

  * Install (as root) installer onto microSD card
    (replace SDCARD with actual device):

        zcat content/images/transit/transit-lime2-buster.img.gz > /dev/SDCARD

  * Boot box with SDCARD plugged in
    (will migrate, resize and personalize at first boot)

-->
