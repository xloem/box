#!/bin/sh
#
# multistrap-configure.sh
# Copyright 2015 Jonas Smedegaard <dr@jones.dk>
# License: GNU Public License version 3 or newer
#
# multistrap configscript invoked in rootfs to preseed and configure
#
# Recommends: debconf libfakeroot
#
# TODO: avoid need for libfakeroot installed into target system
# TODO: drop dash hack when 0.5.10.2-4 is in oldstable

set -eu

set -x

# silence locale noise (esp. from perl)
export LC_ALL=C.UTF-8 LANGUAGE=C.UTF-8 LANG=C.UTF-8 \

# suppress debconf questions not preseeded
export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

# dirty hack to make dash postinst succeed
[ ! -e /var/lib/dpkg/info/dash.preinst ] \
	|| [ -n "$(dpkg-divert --list /bin/sh)" ] \
	|| /var/lib/dpkg/info/dash.preinst

# preseed debconf questions
cat /tmp/preseeds/* 2>&- | debconf-set-selections

cat <<'EOF' >debconf-get
#!/bin/sh
# multistrap debconf wrapper to mimic d-i convenience command
set -eu
debconf-show d-i | grep -Po "(?<=. $1: ).*"
EOF
chmod +x debconf-get

# add hostname file
# FIXME: resolve from debconf
cat <<'EOF' > /etc/hostname
box
EOF

# add fstab file
# FIXME: resolve from debconf
# TODO: use UUID
# : enable swap
cat <<'EOF' > /etc/fstab
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# / was on /dev/mmcblk0p2 during installation
/dev/mmcblk0p2 /               ext4    errors=remount-ro 0       1
# /boot was on /dev/mmcblk0p1 during installation
/dev/mmcblk0p1 /boot           ext4    defaults        0       2
# swap was on /dev/mmcblk0p5 during installation
#/dev/mmcblk0p5 none            swap    sw              0       0

tmpfs    /tmp        tmpfs    defaults    0 0
tmpfs    /var/tmp    tmpfs    defaults    0 0
EOF

# passify flash-kernel, triggered by initramfs-tools hook on some archs
export FK_MACHINE=none

# workaround for base-files/base-passwd race condition (see bug#924401)
# TODO: instead auto-resolve dependencies of, and configure, base-passwd
sed -i.orig -E 's/ root:/ 0:/;s/:root /:0 /;s/:staff /:50 /;s/:utmp /:43 /;s/ ([0-9]+) root$/ \1 0/;s/ ([0-9]+) mail$/ \1 8/' /var/lib/dpkg/info/base-files.postinst
#dpkg --configure gcc-8-base libgcc1 libc6 libdebconfclient0 base-passwd

# avoid generate initramfs twice: temporarily avoid initramfs-tools hook
chmod -x /etc/kernel/postinst.d/initramfs-tools

# configure packages
dpkg --configure -a

# avoid generate initramfs twice: re-enable initramfs-tools hook
chmod +x /etc/kernel/postinst.d/initramfs-tools
update-initramfs -c -k "$(linux-version list | linux-version sort | tail --lines 1)"
[ ! -x /usr/sbin/u-boot-update ] || u-boot-update

# workaround for base-files/base-passwd race condition (see bug#924401)
# TODO: drop when instead applying workaround to base-passwd (see above)
mv -f /var/lib/dpkg/info/base-files.postinst.orig /var/lib/dpkg/info/base-files.postinst

./debconf-get preseed/late_command \
	| PATH="$PATH:." perl -0777 -n \
		-e 's,chroot\s+/target\s+,,g;' \
		-e 's,/target/,/,g;' \
		-e "s,'',,g;" \
		-e 's,^\s+,,;' \
		-e 'system($_) == 0 or die "Broken late_command:\n$_" if ($_);'

rm -f ./debconf-get

# clean package lists and apt cache
apt-get --option Dir::Etc::SourceList=/dev/null update

# purge cached data known to be recreated as needed
rm -rf /var/cache/debconf

# purge seemingly locally generated apt keyring file
rm -f /etc/apt/trusted.gpg.d/debian-archive-keyring.gpg

echo 'multistrap-configure.sh completed succesfully!' >&2
