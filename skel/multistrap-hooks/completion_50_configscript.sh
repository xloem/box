#!/bin/sh
#
# completion_50_configscript.sh
# Copyright 2015 Jonas Smedegaard <dr@jones.dk>
# License: GNU Public License version 3 or newer
#
# multistrap completion hook supporting alien arch with fakeroot
#
# Recommends: fakeroot proot qemu-user
# Suggests: arch-test qemu-user-static
# Enhances: multistrap
#
# TODO: avoid need for libfakeroot installed into target system
# TODO: avoid install static QEMU into target system when run as real root
# TODO: optimize by having proot bypass select binaries
#  candidates:
#  * /bin/sh
#  * /usr/bin/perl
#  * /usr/bin/mandb
#  * /usr/sbin/update-ca-certificates
#  * /usr/bin/c_rehash

set -eu
set -x

fs="$1"
# TODO: replace with multiarch-safe method
arch=$(grep -Pom1 '(?<=Architecture: )(?!all)(.*)' "$fs/var/lib/dpkg/available") #'
arch_host=$(dpkg --print-architecture)

# libssl needs either HOME or RANDFILE defined
export RANDFILE=/root/.rnd.installer

# Favor dpkg-dev tools (most reliable but pulls many packages)
if which dpkg-architecture; then
	multiarch=$(dpkg-architecture -a"$arch" -qDEB_TARGET_MULTIARCH)
	cpu=$(dpkg-architecture -a"$arch" -qDEB_TARGET_GNU_CPU)
else
	multiarch=$(basename "$(dirname "$(find "$fs/usr/lib" -type d -name libfakeroot)")")
	cpu=$(echo "$multiarch" | cut -d- -f1)
fi
if which arch-test && arch-test -n "$arch"; then
	qemu=
else
	qemu=$(/usr/sbin/update-binfmts --display "qemu-$cpu" | grep -Po '(?<=interpreter = /usr/bin/).*') #'
fi
if [ -n "${FAKEROOTKEY:-}" ] && [ "${FAKECHROOT:-}" != "true" ]; then
	PATH=/usr/sbin:/usr/bin:/sbin:/bin \
		LD_LIBRARY_PATH="${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}/usr/lib/$multiarch/libfakeroot" \
		QEMU_LD_PREFIX=${qemu:+"$fs"} \
		proot -r "$fs" -b /proc -b /sys -b /dev ${qemu:+-q "$qemu"} -w / /multistrap-configure.sh
	rm -f "$fs/multistrap-configure.sh"
	# workaround for ca-certificates broken with QEMU (see bug#923479)
	[ -z "$qemu" ] || [ ! -x "$fs/usr/sbin/update-ca-certificates" ] \
		|| PATH=/usr/sbin:/usr/bin:/sbin:/bin \
		LD_LIBRARY_PATH="${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}/usr/lib/$multiarch/libfakeroot" \
		QEMU_LD_PREFIX="$fs" \
		proot -r "$fs" -b /proc -b /sys -b /dev -b /usr/bin/openssl -q "$qemu" openssl rehash -v /etc/ssl/certs
else
	sudo=sudo
	if [ "$(whoami)" = "root" ]; then
		sudo=
	fi
	$sudo mount --bind /proc "$fs/proc"
	$sudo mount --bind /sys "$fs/sys"
	$sudo mount --bind /dev "$fs/dev"
	[ -z "$qemu" ] || cp "/usr/bin/$qemu" "$fs/usr/bin/"
	$sudo chroot "$fs" /multistrap-configure.sh
	[ -z "$qemu" ] || rm "$fs/usr/bin/$qemu"
	# workaround for ca-certificates broken with QEMU (see bug#923479)
	[ -z "$qemu" ] || [ ! -x "$fs/usr/sbin/update-ca-certificates" ] \
		|| PATH=/usr/sbin:/usr/bin:/sbin:/bin \
		QEMU_LD_PREFIX="$fs" \
		$sudo proot -r "$fs" -b /proc -b /sys -b /dev -b /usr/bin/openssl -q "$qemu" openssl rehash -v /etc/ssl/certs
	$sudo umount "$fs/proc"
	$sudo umount "$fs/sys"
	$sudo umount "$fs/dev"
	$sudo rm -f "$fs/multistrap-configure.sh"
fi

rm -f "$fs/$RANDFILE"
