#!/bin/sh
#
# completion_90_chroot_cleanup.sh
# Copyright 2015 Jonas Smedegaard <dr@jones.dk>
# License: GNU Public License version 3 or newer
#
# multistrap completion hook to cleanup chroot hack
#
# Enhances: multistrap

set -eu

fs="$1"

# rollback /usr/share/multistrap/chroot.sh hacks
for file in /sbin/start-stop-daemon /sbin/initctl; do
	[ ! -e "$fs/$file.REAL" ] || mv -f "$fs/$file.REAL" "$fs/$file"
done
for file in /usr/sbin/policy-rc.d; do
	[ -n "$(dpkg -S "$fs/$file" 2>/dev/null)" ] || rm -f "$fs/$file"
done
